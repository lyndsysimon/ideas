# Lyndsy's Project Ideas

This is intended to be a collection of ideas that I've had that I may or may not
ever actually act upon, but wanted to record somewhere so I can refer to them in
the future.

If you see an idea here that you'd like to "claim", by all means submit a PR to
link to your project.

## Mobile apps
* [A remote IDE for tablets](remote_ide.md)

## Namecoin
* [A means of routing Namecoin's .bit TLD, usable by the general public](namecoin_dot_bit_resolution.md)
* [A registrar for the .bit TLD](dot_bit_registrar.md)

## Web Applications
* [A service that generates usernames and emails for online accounts](username_manager.md)
* [A consolidated feed across multiple social media platforms](social_feed.md)
* [A service to infer user metadata](user_signup_metadata.md)
* [An app to visualize deployed AWS assets](aws_visualization.md)

## Python Projects
* [Property testing based on type annotations](python_property_testing_annotations.md)

## Ruby Gems
* [CSV Parser](csv_parser.md)

## Business Ideas
* [AI-enabled interactive epitaphs](ai_epitaphs.md)
* [Privacy-focused personal cloud appliance](personal_cloud.rst)
