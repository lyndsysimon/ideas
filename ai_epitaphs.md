# AI-enabled Interactive Epitaphs

## Ideal Future Use Case

A person walks up to a headstone in a cemetery. They are able to speak "to" their ancestor through a device embedded into the stone itself. The AI powering it draws upon a corpus of the deceased's social media posts and perhaps even their personal correspondenc.
