Password managers help you generate a secure password - what about generating
a secure username that isn't tied to you as an individual? You could set up a
single domain and autogenerate emails within it to use as usernames. "Actual"
names could be derived from a list of English (or other) given and family names.
This would let you easily sign up for a service in a more anonymous way.