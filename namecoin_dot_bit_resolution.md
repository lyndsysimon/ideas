Namecoin offers the promise of a decentralized, censorship-proof alternative to
the existing DNS system. That's a Good Thing™, but it's basically theoretical
until there is an easy way for your mom to resolve .bit TLD domains without
having to think about it. This isn't possible today.

There has been a project to do this in the past that was apparently fairly
successful, called [FreeSpeechMe](https://bit.namecoin.org/freespeechme.html).
FSM seems to be dead - it's "being reworked", but the last version of Firefox
it supported was Firefox 33 - FirefoxNightly is on version 57 as of
September 2017. That said, FSM required the client to download the entirety of
the Namecoin blockchain to resolve domains. That's all well and good for the
purposes of ensuring decentralization, but it's not really a viable solution
for the average home user.

Namecoin's domain registrations last for 36k blocks - so perhaps it would be
necessary only to download only the last 36k blocks to ensure successful
routing?

A Firefox plugin is not a great solution either, since it only works with one
browser. It would be better to tie into the user's hosts file or their local
DNS cache.
