# Ruby Gem: CSV Parser

A Ruby gem that defines a small DSL to ease parsing and using CSV files.

A rough draft of the interface:

```ruby
WorkersSchema = CsvSchema do
  # Specify a type (default: :string)
  field :employee_id, type: :integer

  field :first_name

  # Change the name of the column
  field :family_name, from: last_name

  # Validate with a lambda
  field :job_class, validate: ->(x) { x.in? ['cashier', 'manager'] }

  # Validate with a regex
  field :phone, validate: /[0-9]{3}\.[0-9]{3}\.[0-9]{4}/ do |x|
    # Block passed to the definition specific transformation
    x.replace('-', '.')
  end
end

# selectively exclude records
WorkersSchema = WorkersSchema.exclude_if do |record|
  record.job_class == 'manager'
end

file = WorkersSchema.open('/path/to/file')

file.each do |record|
  # Do something with each row
  Worker
end
```

## Features

### Testable

The library should include Rspec integration:

```ruby
describe WorkersSchema do
  context 'with a valid row' do
    let :data_row <<-EOS
      1,"John","Doe","cashier","123.456.7890"
    EOS

    it 'parses the record' do
      expect(record.employee_id).to eq 1
      expect(record.first_name).to eq 'John'
      expect(record.family_name).to eq 'Doe'
      expect(record.job_class).to eq 'cashier'
      expect(record.phone).to eq '123-456-7890'
    end
  end

  it_behaves_like 'these rows are malformed', [
    '-1,"John","Doe","cashier","123.456.7890"',
    '1,"John","Doe","cashier","+1.123.456.7890"',
    '1,"John","Doe","a different class","123.456.7890"',
    '1,"John","","a different class","123.456.7890"'
  ]

  context 'with example input' do
    let :data <<-EOS
      1,"John","Doe","cashier","123.456.7890"
      2,"Jane","Doe","manager","123.456.7890"
      3,"Jim","Doe","cashier","123.456.7890"
    EOS

    it 'excludes managers' do
      expect(records.map(&:employee_id)).to_not include 2
    end
  end
end

```
