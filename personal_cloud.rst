Personal Cloud
==============

A "cloud-in-a-box", preferably hardware with the form factor of something like a
Mac Mini. It should be able to handle most of someone's digitial life, with a
special emphasis on legal protections under the Fourth Amendment.

Offered Services
----------------

* Email
* Calendaring
* Ad Blocking (a'la Pi Hole?)
  * If it's set up as a DMZ like Pi Hole, this would simplify DynDNS issues


Challenges
----------

* Dealing with dynamic IPs (DynDNS?)
* Setting up DNS records for attached domains
* Email spam filtering
