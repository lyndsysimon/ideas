# User Signup Metadata

Sparked from [this HN post](https://news.ycombinator.com/item?id=15661290).

Most applications would benefit from additional contextual information about
their users, but implementing the code necessary to track this information down
would be a significant effort.

This idea is for a service that accepts the information you already have about
a user and uses it to infer as much about the user as possible from
publicly-available sources. This additional informatio could then be used for a
variety of purposes:

* Notification of "influencer" signups, based on:
  * number of <social network> followers.
  * position in an industry (e.g., "writer for Gizmodo").
  * inclusion in a manually-curated list of users.
* Pre-filling user profiles
* Datamining, based on previously data not provided by the user
* Demographic estimation/inference

## Implementation

This is probably best implemented as a stream processing platform, but in that
form would likely have a very limited audience for monetization. Selling
solutions based upon these data would be better than selling the data directly.

[pingpoint](http://pingpoint.io) is an example of a service that could be
offered atop this infrastructure, but I would prefer not to compete with them
since this idea was sparked as a result of their "Ask HN" post.

Perhaps the aforementioned "profile pre-filling" service would be a better
initial service? Demographic inference could also be lucrative, but I suspect
there are already existing mature offerings in that market.

## Input Data

* Email
* Name
* Social accounts
  * IDs
  * Profile links
* System fingerprint
  * User agent string
  * Individual feature detection
  * Example requests
  * IP
* Access times
  * (use to attempt to establish constraints on time zone)

## Considerations

If this service achieved any degree of popularity, opporunities would arise to
infer information between profiles established across sites from multiple
clients. Should the information supplied from the customers themselves be
used for additional inference, and if so, to what extent should that
information be shared?
