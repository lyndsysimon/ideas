# Property testing based on type annotations

This idea came to me after looking at [Ow!](https://github.com/sindresorhus/ow),
which provides funtion argument constraints for TypeScript. In short, I'd like
to be able to use this in Python to generate sane error messages when unexpected
input is encountered, but also to generate appropriate property-based tests by
integrating a tool like [Hypothesis](https://hypothesis.works).
