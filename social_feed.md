# Social Feed

A site where you can follow users on multiple social media platforms, across
potentially multiple user accounts on each.

The idea spawned from a desire to be able to follow someone's Reddit posts who
I also follow on Facebook. He has at least two Reddit accounts that I know of.