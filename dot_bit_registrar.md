Namecoin does not require a registrar as such - but a website where you can
easily pay in USD or another more common means of payment would make it much
easier to register and keep up-to-date a .bit domain registration.

If this could be done without giving the registrar itself the "keys to the
kingdom", that would be even better.

With a reasonable fee for the service, this idea could be profitable (or at
least self-sustaining).
