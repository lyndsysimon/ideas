# AWS Visualization

A web app to generate a live map of assets deployed onto AWS.

Input sources:
  * Cloudformation template(s)
  * AWS credentials
