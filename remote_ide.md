# A Remote IDE for tablets

## Overview

I want PyCharm on my iPad. The biggest thing stopping me today isn't the lack of
an iOS PyCharm app, but the restrictions on iOS itself - inability to run a
local Python interpreter, access a shell, and so on. I want all of that stuff to
happen on a remote server, and the iOS app to connect to it transparently.

## Thoughts

With [Blink](http://blink.sh), I'm able to connect to a remote shell on iOS. It
works well, but I'm limited to using shell tools. Vim can of course be turned
into something resembling an IDE, but that level of customization and learning
curve isn't for everyone.

What if there was an app like PyCharm on iOS? The big difference would be that
the code itself would live on the remote server, along with the interpreter.
The entire project could be a docker container, and all of the cool stuff that
PyCharm does to index the project and provide contextual assistance would run
there as well.

The first step would be to split the UI from the IDE capabilities of PyCharm,
connecting the two with an API. That API could then (optionally?) be running on
a remote server. The next step would be to abstract away the remote server
itself as much as possible, so that creating a new project was done by deploying
a new "base project" image. This would allow the user to get a shell over MOSH
for fiddly commands and such, but the goal would be to eliminate the need to do
so by implementing them in the IDE's API.
